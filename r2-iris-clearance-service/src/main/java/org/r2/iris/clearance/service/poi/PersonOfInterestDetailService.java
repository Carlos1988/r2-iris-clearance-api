package org.r2.iris.clearance.service.poi;

import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDetailDto;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public interface PersonOfInterestDetailService {
    /**
     * Find person of interest detail by id or person of interest id
     *
     * @param id person of interest id
     * @return PersonOfInterestDetailDto - person of interest detail object
     */
    PersonOfInterestDetailDto findById(String id);

    /**
     * Save or update person of interest detail given id or person of interest id
     *
     * @param id  person of interest id
     * @param dto PersonOfInterestDetailDto - person of interest detail object to save
     */
    void saveOrUpdate(String id, PersonOfInterestDetailDto dto);
}
