package org.r2.iris.clearance.service.poi.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class PersonOfInterestIdGenerator implements IdentifierGenerator {
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        LocalDateTime now = LocalDateTime.now();
        return "R2POI-" + now.getYear() + String.format("%02d", now.getMonthValue()) + String.format("%02d", now.getDayOfMonth())
            + "-" + UUID.randomUUID().toString().substring(0, 8);
    }
}
