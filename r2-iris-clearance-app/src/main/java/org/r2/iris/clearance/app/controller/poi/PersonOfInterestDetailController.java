package org.r2.iris.clearance.app.controller.poi;

import org.r2.iris.clearance.service.poi.PersonOfInterestDetailService;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RestController
@RequestMapping("/personsofinterest/{id}/details")
public class PersonOfInterestDetailController {
    @Autowired
    private PersonOfInterestDetailService service;

    @GetMapping
    public PersonOfInterestDetailDto findById(@PathVariable("id") String id) {
        return service.findById(id);
    }

    @PutMapping
    public void saveOrUpdate(@PathVariable("id") String id, @RequestBody PersonOfInterestDetailDto dto) {
        service.saveOrUpdate(id, dto);
    }
}
