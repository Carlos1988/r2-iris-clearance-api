package org.r2.iris.clearance.service.poi.impl;

import org.r2.iris.clearance.service.lookup.dto.AddressDto;
import org.r2.iris.clearance.service.lookup.util.AddressUtil;
import org.r2.iris.clearance.service.poi.PersonOfInterestDetailService;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDetailDto;
import org.r2.iris.clearance.service.poi.model.PersonOfInterest;
import org.r2.iris.clearance.service.poi.model.PersonOfInterestDetail;
import org.r2.iris.clearance.service.poi.repo.PersonOfInterestDetailRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@Service
public class PersonOfInterestDetailServiceImpl implements PersonOfInterestDetailService {
    @Autowired
    private PersonOfInterestDetailRepo repo;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public PersonOfInterestDetailDto findById(String id) {
        PersonOfInterestDetailDto result = null;
        PersonOfInterestDetail model = repo.findOne(id);
        if (model != null) {
            AddressDto birthPlace = AddressUtil.INSTANCE.toAddressDto(model.getBirthPlace());
            AddressDto currentAddress = AddressUtil.INSTANCE.toAddressDto(model.getCurrentAddress());
            AddressDto cityAddress = AddressUtil.INSTANCE.toAddressDto(model.getCityAddress());
            AddressDto provincialAddress = AddressUtil.INSTANCE.toAddressDto(model.getProvincialAddress());
            AddressDto spouseAddress = AddressUtil.INSTANCE.toAddressDto(model.getSpouseAddress());
            AddressDto parentsAddress = AddressUtil.INSTANCE.toAddressDto(model.getParentsAddress());
            result = new PersonOfInterestDetailDto(birthPlace, model.getCivilStatus(), currentAddress, cityAddress, provincialAddress,
                model.getSpouse(), model.getSpouseOccupation(), spouseAddress, model.getFather(), model.getFathersOccupation(),
                model.getMother(), model.getMothersOccupation(), parentsAddress, model.getRelative1(), model.getRelative2(),
                model.getDialects(), model.getCitizenship(), model.getHeight(), model.getWeight(), model.getBuild(),
                model.getReligion(), model.getPhoneNumber(), model.getEmergencyContactPerson(),
                model.getEmergencyContactPersonAddressAndNumber(), model.getHighestEducationalAttainment(),
                model.getColorAndDescOfHair(), model.getColorAndDescOfEyes(), model.getFaceFrontPhoto(), model.getFaceRightPhoto(), model.getFaceLeftPhoto(),
                model.getRightThumbFingerPrint(), model.getRightIndexFingerPrint(), model.getRightMiddleFingerPrint(), model.getRightRingFingerPrint(), model.getRightPinkyFingerPrint(),
                model.getLeftThumbFingerPrint(), model.getLeftIndexFingerPrint(), model.getLeftMiddleFingerPrint(), model.getLeftRingFingerPrint(), model.getLeftPinkyFingerPrint());
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void saveOrUpdate(String id, PersonOfInterestDetailDto dto) {
        PersonOfInterestDetail model = repo.findOne(id);
        if (model == null) {
            model = new PersonOfInterestDetail();
            model.setPerson(new PersonOfInterest(id));
        }
        BeanUtils.copyProperties(dto, model, "birthPlace", "currentAddress", "cityAddress", "provincialAddress", "spouseAddress",
            "parentsAddress");

        model.setBirthPlace(AddressUtil.INSTANCE.toAddress(dto.getBirthPlace()));
        model.setCurrentAddress(AddressUtil.INSTANCE.toAddress(dto.getCurrentAddress()));
        model.setCityAddress(AddressUtil.INSTANCE.toAddress(dto.getCityAddress()));
        model.setProvincialAddress(AddressUtil.INSTANCE.toAddress(dto.getProvincialAddress()));
        model.setSpouseAddress(AddressUtil.INSTANCE.toAddress(dto.getSpouseAddress()));
        model.setParentsAddress(AddressUtil.INSTANCE.toAddress(dto.getParentsAddress()));

        repo.save(model);
    }
}
