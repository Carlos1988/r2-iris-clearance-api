package org.r2.iris.clearance.service.poi.model;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import org.hibernate.envers.Audited;
import org.r2.iris.clearance.service.common.AbstractAuditableEntity;
import org.r2.iris.clearance.service.common.embeddable.Address;
import org.r2.iris.clearance.service.common.embeddable.Name;
import org.r2.iris.clearance.service.poi.model.support.CivilStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@JsonIgnoreProperties({"person", "birthPlace", "currentAddress", "cityAddress", "provincialAddress", "spouseAddress",
    "parentsAddress"})
@Entity
@Audited
public class PersonOfInterestDetail extends AbstractAuditableEntity implements Serializable {
    private static final long serialVersionUID = 4880362752964822060L;
    @Id
    private String id;
    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private PersonOfInterest person;
    @Embedded
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "birth_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "birth_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "birth_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "birth_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "birth_address_otherDetail"))
    })
    private Address birthPlace;
    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private CivilStatus civilStatus;
    @Embedded
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "current_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "current_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "current_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "current_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "current_address_otherDetail"))
    })
    private Address currentAddress;
    @Embedded
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "city_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "city_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "city_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "city_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "city_address_otherDetail"))
    })
    private Address cityAddress;
    @Embedded
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "provincial_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "provincial_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "provincial_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "provincial_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "provincial_address_otherDetail"))
    })
    private Address provincialAddress;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "spouse_first_name")),
        @AttributeOverride(name = "middleName", column = @Column(name = "spouse_middle_name")),
        @AttributeOverride(name = "lastName", column = @Column(name = "spouse_last_name"))
    })
    private Name spouse;
    private String spouseOccupation;
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "spouse_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "spouse_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "spouse_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "spouse_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "spouse_address_otherDetail"))
    })
    private Address spouseAddress;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "father_first_name")),
        @AttributeOverride(name = "middleName", column = @Column(name = "father_middle_name")),
        @AttributeOverride(name = "lastName", column = @Column(name = "father_last_name"))
    })
    private Name father;
    private String fathersOccupation;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "mother_first_name")),
        @AttributeOverride(name = "middleName", column = @Column(name = "mother_middle_name")),
        @AttributeOverride(name = "lastName", column = @Column(name = "mother_last_name"))
    })
    private Name mother;
    private String mothersOccupation;
    @Embedded
    @AssociationOverrides({
        @AssociationOverride(name = "region", joinColumns = @JoinColumn(name = "parents_address_region_id")),
        @AssociationOverride(name = "province", joinColumns = @JoinColumn(name = "parents_address_province_id")),
        @AssociationOverride(name = "municipality", joinColumns = @JoinColumn(name = "parents_address_municipality_id")),
        @AssociationOverride(name = "barangay", joinColumns = @JoinColumn(name = "parents_address_barangay_id"))
    })
    @AttributeOverrides({
        @AttributeOverride(name = "otherDetail", column = @Column(name = "parents_address_otherDetail"))
    })
    private Address parentsAddress;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "relative1_first_name")),
        @AttributeOverride(name = "middleName", column = @Column(name = "relative1_middle_name")),
        @AttributeOverride(name = "lastName", column = @Column(name = "relative1_last_name"))
    })
    private Name relative1;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "firstName", column = @Column(name = "relative2_first_name")),
        @AttributeOverride(name = "middleName", column = @Column(name = "relative2_middle_name")),
        @AttributeOverride(name = "lastName", column = @Column(name = "relative2_last_name"))
    })
    private Name relative2;
    private String dialects;
    private String citizenship;
    private String height;
    private String weight;
    private String build;
    private String religion;
    private String phoneNumber;
    private String emergencyContactPerson;
    private String emergencyContactPersonAddressAndNumber;
    private String highestEducationalAttainment;
    private String colorAndDescOfHair;
    private String colorAndDescOfEyes;
    private String faceFrontPhoto;
    private String faceRightPhoto;
    private String faceLeftPhoto;
    private String rightThumbFingerPrint;
    private String rightIndexFingerPrint;
    private String rightMiddleFingerPrint;
    private String rightRingFingerPrint;
    private String rightPinkyFingerPrint;
    private String leftThumbFingerPrint;
    private String leftIndexFingerPrint;
    private String leftMiddleFingerPrint;
    private String leftRingFingerPrint;
    private String leftPinkyFingerPrint;
    
    public String getId() {
        return id;
    }

    public PersonOfInterest getPerson() {
        return person;
    }

    public void setPerson(PersonOfInterest person) {
        this.person = person;
        this.id = person != null ? person.getId() : null;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Address getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(Address birthPlace) {
        this.birthPlace = birthPlace;
    }

    public CivilStatus getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(CivilStatus civilStatus) {
        this.civilStatus = civilStatus;
    }

    public Address getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(Address currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Address getCityAddress() {
        return cityAddress;
    }

    public void setCityAddress(Address cityAddress) {
        this.cityAddress = cityAddress;
    }

    public Address getProvincialAddress() {
        return provincialAddress;
    }

    public void setProvincialAddress(Address provincialAddress) {
        this.provincialAddress = provincialAddress;
    }

    public Name getSpouse() {
        return spouse;
    }

    public void setSpouse(Name spouse) {
        this.spouse = spouse;
    }

    public String getSpouseOccupation() {
        return spouseOccupation;
    }

    public void setSpouseOccupation(String spouseOccupation) {
        this.spouseOccupation = spouseOccupation;
    }

    public Address getSpouseAddress() {
        return spouseAddress;
    }

    public void setSpouseAddress(Address spouseAddress) {
        this.spouseAddress = spouseAddress;
    }

    public Name getFather() {
        return father;
    }

    public void setFather(Name father) {
        this.father = father;
    }

    public String getFathersOccupation() {
        return fathersOccupation;
    }

    public void setFathersOccupation(String fathersOccupation) {
        this.fathersOccupation = fathersOccupation;
    }

    public Name getMother() {
        return mother;
    }

    public void setMother(Name mother) {
        this.mother = mother;
    }

    public String getMothersOccupation() {
        return mothersOccupation;
    }

    public void setMothersOccupation(String mothersOccupation) {
        this.mothersOccupation = mothersOccupation;
    }

    public Address getParentsAddress() {
        return parentsAddress;
    }

    public void setParentsAddress(Address parentsAddress) {
        this.parentsAddress = parentsAddress;
    }

    public Name getRelative1() {
        return relative1;
    }

    public void setRelative1(Name relative1) {
        this.relative1 = relative1;
    }

    public Name getRelative2() {
        return relative2;
    }

    public void setRelative2(Name relative2) {
        this.relative2 = relative2;
    }

    public String getDialects() {
        return dialects;
    }

    public void setDialects(String dialects) {
        this.dialects = dialects;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmergencyContactPerson() {
        return emergencyContactPerson;
    }

    public void setEmergencyContactPerson(String emergencyContactPerson) {
        this.emergencyContactPerson = emergencyContactPerson;
    }

    public String getEmergencyContactPersonAddressAndNumber() {
        return emergencyContactPersonAddressAndNumber;
    }

    public void setEmergencyContactPersonAddressAndNumber(String emergencyContactPersonAddressAndNumber) {
        this.emergencyContactPersonAddressAndNumber = emergencyContactPersonAddressAndNumber;
    }

    public String getHighestEducationalAttainment() {
        return highestEducationalAttainment;
    }

    public void setHighestEducationalAttainment(String highestEducationalAttainment) {
        this.highestEducationalAttainment = highestEducationalAttainment;
    }

    public String getColorAndDescOfHair() {
        return colorAndDescOfHair;
    }

    public void setColorAndDescOfHair(String colorAndDescOfHair) {
        this.colorAndDescOfHair = colorAndDescOfHair;
    }

    public String getColorAndDescOfEyes() {
        return colorAndDescOfEyes;
    }

    public void setColorAndDescOfEyes(String colorAndDescOfEyes) {
        this.colorAndDescOfEyes = colorAndDescOfEyes;
    }

	public String getFaceFrontPhoto() {
		return faceFrontPhoto;
	}

	public void setFaceFrontPhoto(String faceFrontPhoto) {
		this.faceFrontPhoto = faceFrontPhoto;
	}

	public String getFaceRightPhoto() {
		return faceRightPhoto;
	}

	public void setFaceRightPhoto(String faceRightPhoto) {
		this.faceRightPhoto = faceRightPhoto;
	}

	public String getFaceLeftPhoto() {
		return faceLeftPhoto;
	}

	public void setFaceLeftPhoto(String faceLeftPhoto) {
		this.faceLeftPhoto = faceLeftPhoto;
	}

	public String getRightThumbFingerPrint() {
		return rightThumbFingerPrint;
	}

	public void setRightThumbFingerPrint(String rightThumbFingerPrint) {
		this.rightThumbFingerPrint = rightThumbFingerPrint;
	}

	public String getRightIndexFingerPrint() {
		return rightIndexFingerPrint;
	}

	public void setRightIndexFingerPrint(String rightIndexFingerPrint) {
		this.rightIndexFingerPrint = rightIndexFingerPrint;
	}

	public String getRightMiddleFingerPrint() {
		return rightMiddleFingerPrint;
	}

	public void setRightMiddleFingerPrint(String rightMiddleFingerPrint) {
		this.rightMiddleFingerPrint = rightMiddleFingerPrint;
	}

	public String getRightRingFingerPrint() {
		return rightRingFingerPrint;
	}

	public void setRightRingFingerPrint(String rightRingFingerPrint) {
		this.rightRingFingerPrint = rightRingFingerPrint;
	}

	public String getRightPinkyFingerPrint() {
		return rightPinkyFingerPrint;
	}

	public void setRightPinkyFingerPrint(String rightPinkyFingerPrint) {
		this.rightPinkyFingerPrint = rightPinkyFingerPrint;
	}

	public String getLeftThumbFingerPrint() {
		return leftThumbFingerPrint;
	}

	public void setLeftThumbFingerPrint(String leftThumbFingerPrint) {
		this.leftThumbFingerPrint = leftThumbFingerPrint;
	}

	public String getLeftIndexFingerPrint() {
		return leftIndexFingerPrint;
	}

	public void setLeftIndexFingerPrint(String leftIndexFingerPrint) {
		this.leftIndexFingerPrint = leftIndexFingerPrint;
	}

	public String getLeftMiddleFingerPrint() {
		return leftMiddleFingerPrint;
	}

	public void setLeftMiddleFingerPrint(String leftMiddleFingerPrint) {
		this.leftMiddleFingerPrint = leftMiddleFingerPrint;
	}

	public String getLeftRingFingerPrint() {
		return leftRingFingerPrint;
	}

	public void setLeftRingFingerPrint(String leftRingFingerPrint) {
		this.leftRingFingerPrint = leftRingFingerPrint;
	}

	public String getLeftPinkyFingerPrint() {
		return leftPinkyFingerPrint;
	}

	public void setLeftPinkyFingerPrint(String leftPinkyFingerPrint) {
		this.leftPinkyFingerPrint = leftPinkyFingerPrint;
	}
}
