package org.r2.iris.clearance.app.exception;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class DuplicateRecordException extends RuntimeException {
    public DuplicateRecordException(String message) {
        super(message);
    }
}
