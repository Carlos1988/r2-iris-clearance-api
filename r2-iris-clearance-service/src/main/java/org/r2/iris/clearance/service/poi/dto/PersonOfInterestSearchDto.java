package org.r2.iris.clearance.service.poi.dto;

import java.util.Date;

import org.r2.iris.clearance.service.poi.model.support.Gender;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class PersonOfInterestSearchDto {
    /**
     * Search firstname or middlename or lastname
     */
    private String name;
    private String firstName;
    private String middleName;
    private String lastName;
    private Gender gender;
    private Date birthDate;

    public PersonOfInterestSearchDto() {
    }

    public PersonOfInterestSearchDto(String name) {
        this.name = name;
    }

    public PersonOfInterestSearchDto(String firstName, String middleName, String lastName, Gender gender, Date birthDate) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonOfInterestSearchDto that = (PersonOfInterestSearchDto) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (gender != that.gender) return false;
        return birthDate != null ? birthDate.equals(that.birthDate) : that.birthDate == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        return result;
    }
}
