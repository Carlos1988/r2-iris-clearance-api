package org.r2.iris.clearance.service.poi.dto;

import java.util.Date;

import org.r2.iris.clearance.service.common.AbstractAuditableEntity;
import org.r2.iris.clearance.service.util.DateUtil;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class AbstractAuditableDto {
    // To be consistent, use java.util.Date in all date fields in DTO
    private Date createdDate;
    private Date lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public void copyAuditFields(AbstractAuditableEntity model) {
        setCreatedBy(model.getCreatedBy());
        setCreatedDate(DateUtil.INSTANCE.toDate(model.getCreatedDate()));
        setLastModifiedBy(model.getLastModifiedBy());
        setLastModifiedDate(DateUtil.INSTANCE.toDate(model.getLastModifiedDate()));
    }
}
