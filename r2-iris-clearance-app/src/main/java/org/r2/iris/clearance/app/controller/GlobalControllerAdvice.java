package org.r2.iris.clearance.app.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.r2.iris.clearance.app.propertyeditor.DatePropertyEditorSupport;
import org.r2.iris.clearance.app.exception.DuplicateRecordException;
import org.r2.iris.clearance.app.exception.ValidationException;
import org.r2.iris.clearance.service.exception.GenericServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@ControllerAdvice
public class GlobalControllerAdvice {
    @InitBinder
    public void registerCustomEditors(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new DatePropertyEditorSupport());
    }

    /**
     * This method basically just serializes the exception to JSON
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public Map<String, Object> handleValidationError(ValidationException exception) {
        /* Default spring error format
        {
            "timestamp": 1482992534992,
            "status": 500,
            "error": "Internal Server Error",
            "exception": "java.lang.IllegalArgumentException",
            "message": "Some Exception",
            "path": "/ciwatchlists/1"
        }
         */
        Map<String, Object> result = new HashMap<>();
        result.put("timestamp", new Date());
        result.put("status", HttpStatus.BAD_REQUEST.value());
        result.put("error", HttpStatus.BAD_REQUEST.getReasonPhrase());
        result.put("exception", exception.getClass().toString());
        result.put("message", exception.getMessage());
        result.put("fieldErrors", exception.getFieldErrors());
        return result;
    }

    /**
     * This method basically just serializes the exception to JSON
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({DuplicateRecordException.class, GenericServiceException.class})
    @ResponseBody
    public Map<String, Object> handleExceptionError(Exception exception) {
        Map<String, Object> result = new HashMap<>();
        result.put("timestamp", new Date());
        result.put("status", HttpStatus.BAD_REQUEST.value());
        result.put("error", HttpStatus.BAD_REQUEST.getReasonPhrase());
        result.put("exception", exception.getClass().toString());
        result.put("message", exception.getMessage());
        return result;
    }
}