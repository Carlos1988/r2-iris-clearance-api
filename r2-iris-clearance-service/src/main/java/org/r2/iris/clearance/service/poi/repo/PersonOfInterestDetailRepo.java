package org.r2.iris.clearance.service.poi.repo;


import org.r2.iris.clearance.service.poi.model.PersonOfInterestDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public interface PersonOfInterestDetailRepo
    extends JpaRepository<PersonOfInterestDetail, String>, QueryDslPredicateExecutor<PersonOfInterestDetail> {
}
