package org.r2.iris.clearance.app.exception;


import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
    getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
public class ValidationException extends Exception {
    @JsonProperty
    private Map<String, String> fieldErrors = Maps.newHashMap();

    @JsonProperty
    private String message;

    public ValidationException() {
    }

    public ValidationException(BindingResult bindingResult) {
        StringBuilder messageBuilder = new StringBuilder();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        int i = 0;
        for (; i < fieldErrors.size() - 1; i++) {
            FieldError error = fieldErrors.get(i);
            messageBuilder.append(error.getField());
            messageBuilder.append(" ");
            messageBuilder.append(error.getDefaultMessage());
            messageBuilder.append("\n");
            this.fieldErrors.put(error.getField(), error.getDefaultMessage());
        }
        FieldError error = fieldErrors.get(i);
        messageBuilder.append(error.getField());
        messageBuilder.append(" ");
        messageBuilder.append(error.getDefaultMessage());
        this.fieldErrors.put(error.getField(), error.getDefaultMessage());

        this.message = messageBuilder.toString();
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    @Override
    public String getMessage() {
        return message;
    }
}