package org.r2.iris.clearance.service.poi.impl;


import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.r2.iris.clearance.service.exception.GenericServiceException;
import org.r2.iris.clearance.service.poi.PersonOfInterestService;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDto;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestSearchDto;
import org.r2.iris.clearance.service.poi.model.PersonOfInterest;
import org.r2.iris.clearance.service.poi.model.QPersonOfInterest;
import org.r2.iris.clearance.service.poi.repo.PersonOfInterestRepo;
import org.r2.iris.clearance.service.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringPath;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@Service
public class PersonOfInterestServiceImpl implements PersonOfInterestService {
    @Autowired
    private PersonOfInterestRepo repo;
    private Converter<PersonOfInterest, PersonOfInterestDto> converter = PersonOfInterestDto::fromModel;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public PersonOfInterestDto findById(String id) {
        PersonOfInterest model = repo.findOne(id);
        return converter.convert(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonOfInterestDto> findAll(PersonOfInterestSearchDto dto, int page, int size) {
        BooleanExpression expression = createExpression(QPersonOfInterest.personOfInterest, dto);
        if (expression != null) {
            return repo.findAll(expression, new PageRequest(page, size,
                new Sort(new Sort.Order(Sort.Direction.ASC, "lastName")))).map(converter);
        } else {
            return repo.findAll(new PageRequest(page, size,
                new Sort(new Sort.Order(Sort.Direction.ASC, "lastName")))).map(converter);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public boolean exists(String id) {
        return repo.exists(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public boolean exists(PersonOfInterestDto dto) {
        QPersonOfInterest qPerson = QPersonOfInterest.personOfInterest;
        List<BooleanExpression> expressions = Lists.newArrayList();
        if (StringUtils.isNotBlank(dto.getFirstName())) {
            expressions.add(qPerson.firstName.equalsIgnoreCase(dto.getFirstName()));
        }
        if (StringUtils.isNotBlank(dto.getMiddleName())) {
            expressions.add(qPerson.middleName.equalsIgnoreCase(dto.getMiddleName()));
        }
        if (StringUtils.isNotBlank(dto.getLastName())) {
            expressions.add(qPerson.lastName.equalsIgnoreCase(dto.getLastName()));
        }
        if (dto.getGender() != null) {
            expressions.add(qPerson.gender.eq(dto.getGender()));
        }
        if (dto.getBirthDate() != null) {
            expressions.add(
                qPerson.birthDate.goe(DateUtil.INSTANCE.sod(dto.getBirthDate()))
                    .and(qPerson.birthDate.lt(DateUtil.INSTANCE.nextDay(dto.getBirthDate())))
            );
        }

        return expressions.size() < 1 // Mark PersoOfInterest as existing if all of the fields above are not supplied.
            || repo.exists(Expressions.allOf(expressions.toArray(new BooleanExpression[expressions.size()])));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public boolean exists(PersonOfInterestDto dto, String excludeId) {
        QPersonOfInterest qPerson = QPersonOfInterest.personOfInterest;
        List<BooleanExpression> expressions = Lists.newArrayList();
        expressions.add(qPerson.id.ne(excludeId));
        if (StringUtils.isNotBlank(dto.getFirstName())) {
            expressions.add(qPerson.firstName.equalsIgnoreCase(dto.getFirstName()));
        }
        if (StringUtils.isNotBlank(dto.getMiddleName())) {
            expressions.add(qPerson.middleName.equalsIgnoreCase(dto.getMiddleName()));
        }
        if (StringUtils.isNotBlank(dto.getLastName())) {
            expressions.add(qPerson.lastName.equalsIgnoreCase(dto.getLastName()));
        }
        if (dto.getGender() != null) {
            expressions.add(qPerson.gender.eq(dto.getGender()));
        }
        if (dto.getBirthDate() != null) {
            expressions.add(
                qPerson.birthDate.goe(DateUtil.INSTANCE.sod(dto.getBirthDate()))
                    .and(qPerson.birthDate.lt(DateUtil.INSTANCE.nextDay(dto.getBirthDate())))
            );
        }
        return repo.exists(Expressions.allOf(expressions.toArray(new BooleanExpression[expressions.size()])));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public PersonOfInterest save(PersonOfInterestDto dto) {
        return repo.save(dto.toPerson());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void update(String id, PersonOfInterestDto dto) {
        if (!Optional.ofNullable(id).isPresent()) {
            throw new GenericServiceException("Failed to update Person. Id is required.");
        }
        PersonOfInterest model = repo.findOne(id);
        if (model == null) {
            throw new GenericServiceException("Person with id " + id + " does not exists.");
        }
        if (!dto.equals(PersonOfInterestDto.fromModel(model))) {
            model.setFirstName(dto.getFirstName());
            model.setMiddleName(dto.getMiddleName());
            model.setLastName(dto.getLastName());
            model.setGender(dto.getGender());
            model.setBirthDate(dto.getBirthDate());
            model.setAlias(dto.getAlias());
            repo.save(model);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BooleanExpression createExpression(QPersonOfInterest qPerson, PersonOfInterestSearchDto dto) {
        BooleanExpression expression = null;
        if (StringUtils.isNotBlank(dto.getName())) {
            // Person that contains (ignorecase) the given name in either firstname, middlename, lastname
            String name = dto.getName();
            expression = qPerson.lastName.containsIgnoreCase(name).or(qPerson.firstName.containsIgnoreCase(name))
                .or(qPerson.middleName.containsIgnoreCase(name));
        } else {
            BiFunction<Pair<BooleanExpression, StringPath>, String, BooleanExpression> createExpression = (pair, value) -> {
                if (StringUtils.isNotBlank(value)) {
                    if (pair.getLeft() != null) {
                        return pair.getLeft().and(pair.getRight().containsIgnoreCase(value));
                    } else {
                        return pair.getRight().containsIgnoreCase(value);
                    }
                } else {
                    return pair.getLeft();
                }
            };

            expression = createExpression.apply(ImmutablePair.of(expression, qPerson.firstName), dto.getFirstName());
            expression = createExpression.apply(ImmutablePair.of(expression, qPerson.middleName), dto.getMiddleName());
            expression = createExpression.apply(ImmutablePair.of(expression, qPerson.lastName), dto.getLastName());
            if (dto.getBirthDate() != null) {
                Date sod = DateUtil.INSTANCE.sod(dto.getBirthDate());
                Date nextDay = DateUtil.INSTANCE.nextDay(sod);
                BooleanExpression exp = qPerson.birthDate.goe(sod).and(qPerson.birthDate.lt(nextDay));
                expression = expression != null ? expression.and(exp) : exp;
            }
            if (dto.getGender() != null) {
                BooleanExpression exp = qPerson.gender.eq(dto.getGender());
                expression = expression != null ? expression.and(exp) : exp;
            }
        }
        return expression;
    }
}

