package org.r2.iris.clearance.app.dto;

import java.util.List;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class PagingResult<T> {
    private List<T> list;
    private long total;

    public PagingResult(List<T> list, long total) {
        this.list = list;
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public long getTotal() {
        return total;
    }
}
