package org.r2.iris.clearance.service.poi.dto;

import java.util.Date;

import org.r2.iris.clearance.service.poi.model.PersonOfInterest;
import org.r2.iris.clearance.service.poi.model.support.Gender;
import org.r2.iris.clearance.service.validator.constraints.CheckAtLeastOneNotNull;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@CheckAtLeastOneNotNull(fieldNames = {"firstName", "lastName", "alias"}, message = "Either firstname, lastname or alias must be set.")
public class PersonOfInterestDto extends AbstractAuditableDto {
    private String id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Gender gender;
    private Date birthDate;
    private String alias;

    public PersonOfInterestDto() {
        // Default constructor
    }

    public PersonOfInterestDto(String id) {
        this.id = id;
    }

    public static PersonOfInterestDto fromModel(PersonOfInterest person) {
        PersonOfInterestDto dto = null;
        if (person != null) {
            dto = new PersonOfInterestDto(person.getId());
            dto.firstName = person.getFirstName();
            dto.middleName = person.getMiddleName();
            dto.lastName = person.getLastName();
            dto.gender = person.getGender();
            dto.birthDate = person.getBirthDate();
            dto.alias = person.getAlias();
            dto.copyAuditFields(person);
        }
        return dto;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public PersonOfInterest toPerson() {
        PersonOfInterest person = new PersonOfInterest(id);
        person.setFirstName(firstName);
        person.setMiddleName(middleName);
        person.setLastName(lastName);
        person.setGender(gender);
        person.setBirthDate(birthDate);
        person.setAlias(alias);
        return person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonOfInterestDto that = (PersonOfInterestDto) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (gender != that.gender) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        return alias != null ? alias.equals(that.alias) : that.alias == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        return result;
    }
}
