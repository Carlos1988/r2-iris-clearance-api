package org.r2.iris.clearance.service.lookup.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@Entity
@Table(name = "municipalities")
public class Municipality extends AbstractAddress {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id", nullable = false)
    private Province province;
    @OneToMany(mappedBy = "municipality")
    private List<Barangay> barangays;
    @Type(type = "yes_no")
    @Column(name = "is_city", nullable = false, length = 1)
    private boolean city = false;

    public boolean isCity() {
        return city;
    }

    public void setCity(boolean city) {
        this.city = city;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public List<Barangay> getBarangays() {
        return barangays;
    }

    public void setBarangays(List<Barangay> barangays) {
        this.barangays = barangays;
    }
}
