package org.r2.iris.clearance.service.poi.model.support;

import org.r2.iris.clearance.service.exception.GenericServiceException;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public enum CivilStatus {
    // legally separated is married
    S("SINGLE"), M("MARRIED"), D("DIVORCED"), W("WIDOWED");
    String text;

    CivilStatus(String text) {
        this.text = text;
    }

    @JsonCreator
    public static CivilStatus toCivilStatus(String str) {
        str = str.toUpperCase();
        if (S.text.equalsIgnoreCase(str) || "S".equalsIgnoreCase(str)) {
            return S;
        } else if (M.text.equalsIgnoreCase(str) || "M".equalsIgnoreCase(str)) {
            return M;
        } else if (D.text.equalsIgnoreCase(str) || "D".equalsIgnoreCase(str)) {
            return D;
        } else if (W.text.equalsIgnoreCase(str) || "W".equalsIgnoreCase(str)) {
            return W;
        } else {
            throw new GenericServiceException("Cannot convert text [" + str + "] to Gender enum");
        }
    }
}