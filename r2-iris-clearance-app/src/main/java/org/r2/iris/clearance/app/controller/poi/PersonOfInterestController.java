package org.r2.iris.clearance.app.controller.poi;

import org.r2.iris.clearance.app.dto.PagingResult;
//import org.r2.iris.clearance.service.poi.PersonOfInterestAggregatorService;
import org.r2.iris.clearance.service.poi.PersonOfInterestService;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDto;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestSearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@RestController
@RequestMapping("/personsofinterest")
public class PersonOfInterestController {
    @Autowired
    private PersonOfInterestService service;
//    @Autowired
//    private PersonOfInterestAggregatorService personOfInterestAggregatorService;

    /**
     * Find all persons.
     *
     * @param page page number
     * @param size total size of records that will be returned. Defaults to 10 if not specified to avoid fetching all records which might sacrifice app performance.
     * @param dto  filter
     * @return list of persons which are equal or less than the size specified
     */
    @GetMapping
    public PagingResult<PersonOfInterestDto> findAll(@RequestParam(value = "page", required = false) Integer page,
        @RequestParam(value = "size", required = false) Integer size,
        PersonOfInterestSearchDto dto) {
        if (page == null) {
            page = 0;
        }
        if (size == null) {
            size = 10;
        }
        Page<PersonOfInterestDto> p = service.findAll(dto, page, size);
        return new PagingResult<>(p.getContent(), p.getTotalElements());
    }

    @GetMapping("/{id}")
    public PersonOfInterestDto findById(@PathVariable("id") String id) {
        return service.findById(id);
    }

    @PostMapping("/exists")
    public boolean exists(@RequestBody PersonOfInterestDto dto) {
        return service.exists(dto);
    }

    @GetMapping("/{id}/exists")
    public boolean exists(@PathVariable("id") String id) {
        return service.exists(id);
    }

//    @PostMapping
//    public IdDto save(@RequestBody PersonOfInterestRequestDto dto) {
//        return new IdDto(personOfInterestAggregatorService.save(dto, dto.getType()).getId());
//    }
//
//    @PutMapping("/{id}")
//    public void update(@PathVariable("id") String id, @RequestBody PersonOfInterestRequestDto dto) {
//        personOfInterestAggregatorService.update(id, dto, dto.getType());
//    }
}
