package org.r2.iris.clearance.service.lookup.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
@MappedSuperclass
public abstract class AbstractAddress {
    @Id
    @Column(name = "id")
    private String code;
    @Column(name = "name", nullable = false)
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
