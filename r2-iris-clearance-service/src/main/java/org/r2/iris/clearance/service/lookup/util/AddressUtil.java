package org.r2.iris.clearance.service.lookup.util;

import org.apache.commons.lang3.StringUtils;
import org.r2.iris.clearance.service.lookup.dto.AddressDto;
import org.r2.iris.clearance.service.lookup.model.Barangay;
import org.r2.iris.clearance.service.lookup.model.Municipality;
import org.r2.iris.clearance.service.lookup.model.Province;
import org.r2.iris.clearance.service.lookup.model.Region;
import org.r2.iris.clearance.service.common.embeddable.Address;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public enum AddressUtil {
    INSTANCE;

    public AddressDto toAddressDto(Address address) {
        if (address == null) {
            return null;
        }

        String regionCode = null;
        String regionName = null;
        String provinceCode = null;
        String provinceName = null;
        String municipalityCode = null;
        String municipalityName = null;
        String barangayCode = null;
        String barangayName = null;
        if (address.getRegion() != null) {
            regionCode = address.getRegion().getCode();
            regionName = address.getRegion().getName();
        }
        if (address.getProvince() != null) {
            provinceCode = address.getProvince().getCode();
            provinceName = address.getProvince().getName();
        }
        if (address.getMunicipality() != null) {
            municipalityCode = address.getMunicipality().getCode();
            municipalityName = address.getMunicipality().getName();
        }
        if (address.getBarangay() != null) {
            barangayCode = address.getBarangay().getCode();
            barangayName = address.getBarangay().getName();
        }
        return new AddressDto(regionCode, regionName, provinceCode, provinceName, municipalityCode,
            municipalityName, barangayCode, barangayName, address.getOtherDetail());
    }

    public Address toAddress(AddressDto dto) {
        Address address = null;
        if (dto != null) {
            address = new Address();
            Region region = null;
            if (StringUtils.isNotBlank(dto.getRegionCode())) {
                region = new Region();
                region.setCode(dto.getRegionCode());
                region.setName(dto.getRegionName());
            }
            Province province = null;
            if (StringUtils.isNotBlank(dto.getProvinceCode())) {
                province = new Province();
                province.setCode(dto.getProvinceCode());
                province.setName(dto.getProvinceName());
                province.setRegion(region);
            }

            Municipality municipality = null;
            if (StringUtils.isNotBlank(dto.getMunicipalityCode())) {
                municipality = new Municipality();
                municipality.setCode(dto.getMunicipalityCode());
                municipality.setName(dto.getMunicipalityName());
                municipality.setProvince(province);
            }

            Barangay barangay = null;
            if (StringUtils.isNotBlank(dto.getBarangayCode())) {
                barangay = new Barangay();
                barangay.setCode(dto.getBarangayCode());
                barangay.setName(dto.getBarangayName());
                barangay.setMunicipality(municipality);
            }

            address.setRegion(region);
            address.setProvince(province);
            address.setMunicipality(municipality);
            address.setBarangay(barangay);
            address.setOtherDetail(dto.getOtherDetail());
        }
        return address;
    }
}
