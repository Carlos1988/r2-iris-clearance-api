package org.r2.iris.clearance.service.poi.repo;

import org.r2.iris.clearance.service.poi.model.PersonOfInterest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public interface PersonOfInterestRepo
    extends JpaRepository<PersonOfInterest, String>, QueryDslPredicateExecutor<PersonOfInterest> {
}
