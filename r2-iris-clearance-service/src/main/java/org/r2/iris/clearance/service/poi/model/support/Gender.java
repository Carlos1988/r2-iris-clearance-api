package org.r2.iris.clearance.service.poi.model.support;

import org.r2.iris.clearance.service.exception.GenericServiceException;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public enum Gender {
    M("male"), F("female");
    String text;

    Gender(String text) {
        this.text = text;
    }

    @JsonCreator
    public static Gender fromString(String string) {
        if ("M".equalsIgnoreCase(string)) {
            return M;
        } else if ("male".equalsIgnoreCase(string)) {
            return M;
        } else if ("F".equalsIgnoreCase(string)) {
            return F;
        } else if ("female".equalsIgnoreCase(string)) {
            return F;
        } else {
            throw new GenericServiceException(string + " has no corresponding value");
        }
    }
}