package org.r2.iris.clearance.service.poi;

import org.r2.iris.clearance.service.poi.dto.PersonOfInterestDto;
import org.r2.iris.clearance.service.poi.dto.PersonOfInterestSearchDto;
import org.r2.iris.clearance.service.poi.model.PersonOfInterest;
import org.r2.iris.clearance.service.poi.model.QPersonOfInterest;
import org.springframework.data.domain.Page;

import com.querydsl.core.types.dsl.BooleanExpression;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public interface PersonOfInterestService {
    Page<PersonOfInterestDto> findAll(PersonOfInterestSearchDto dto, int page, int size);

    PersonOfInterestDto findById(String id);

    boolean exists(String id);

    boolean exists(PersonOfInterestDto dto);

    boolean exists(PersonOfInterestDto dto, String excludeId);

    PersonOfInterest save(PersonOfInterestDto dto);

    void update(String id, PersonOfInterestDto dto);

    BooleanExpression createExpression(QPersonOfInterest qPerson, PersonOfInterestSearchDto dto);
}
