package org.r2.iris.clearance.service.poi.dto;

import org.r2.iris.clearance.service.common.embeddable.Name;
import org.r2.iris.clearance.service.lookup.dto.AddressDto;
import org.r2.iris.clearance.service.poi.model.support.CivilStatus;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class PersonOfInterestDetailDto extends AbstractAuditableDto {
    private AddressDto birthPlace;
    private CivilStatus civilStatus;
    private AddressDto currentAddress;
    private AddressDto cityAddress;
    private AddressDto provincialAddress;
    private Name spouse;
    private String spouseOccupation;
    private AddressDto spouseAddress;
    private Name father;
    private String fathersOccupation;
    private Name mother;
    private String mothersOccupation;
    private AddressDto parentsAddress;
    private Name relative1;
    private Name relative2;
    private String dialects;
    private String citizenship;
    private String height;
    private String weight;
    private String build;
    private String religion;
    private String phoneNumber;
    private String emergencyContactPerson;
    private String emergencyContactPersonAddressAndNumber;
    private String highestEducationalAttainment;
    private String colorAndDescOfHair;
    private String colorAndDescOfEyes;
    private String faceFrontPhoto;
    private String faceRightPhoto;
    private String faceLeftPhoto;
    private String rightThumbFingerPrint;
    private String rightIndexFingerPrint;
    private String rightMiddleFingerPrint;
    private String rightRingFingerPrint;
    private String rightPinkyFingerPrint;
    private String leftThumbFingerPrint;
    private String leftIndexFingerPrint;
    private String leftMiddleFingerPrint;
    private String leftRingFingerPrint;
    private String leftPinkyFingerPrint;

    public PersonOfInterestDetailDto(AddressDto birthPlace, CivilStatus civilStatus,
        AddressDto currentAddress, AddressDto cityAddress, AddressDto provincialAddress, Name spouse, String spouseOccupation,
        AddressDto spouseAddress, Name father, String fathersOccupation, Name mother, String mothersOccupation,
        AddressDto parentsAddress, Name relative1, Name relative2, String dialects, String citizenship, String height,
        String weight, String build, String religion, String phoneNumber, String emergencyContactPerson,
        String emergencyContactPersonAddressAndNumber, String highestEducationalAttainment, String colorAndDescOfHair,
        String colorAndDescOfEyes, String faceFrontPhoto, String faceRightPhoto, String faceLeftPhoto,
        String rightThumbFingerPrint, String rightIndexFingerPrint, String rightMiddleFingerPrint, String rightRingFingerPrint, String rightPinkyFingerPrint,
        String leftThumbFingerPrint, String leftIndexFingerPrint, String leftMiddleFinderPrint, String leftRingFingerPrint, String leftPinkyFingerPrint) {
        this.birthPlace = birthPlace;
        this.civilStatus = civilStatus;
        this.currentAddress = currentAddress;
        this.cityAddress = cityAddress;
        this.provincialAddress = provincialAddress;
        this.spouse = spouse;
        this.spouseOccupation = spouseOccupation;
        this.spouseAddress = spouseAddress;
        this.father = father;
        this.fathersOccupation = fathersOccupation;
        this.mother = mother;
        this.mothersOccupation = mothersOccupation;
        this.parentsAddress = parentsAddress;
        this.relative1 = relative1;
        this.relative2 = relative2;
        this.dialects = dialects;
        this.citizenship = citizenship;
        this.height = height;
        this.weight = weight;
        this.build = build;
        this.religion = religion;
        this.phoneNumber = phoneNumber;
        this.emergencyContactPerson = emergencyContactPerson;
        this.emergencyContactPersonAddressAndNumber = emergencyContactPersonAddressAndNumber;
        this.highestEducationalAttainment = highestEducationalAttainment;
        this.colorAndDescOfHair = colorAndDescOfHair;
        this.colorAndDescOfEyes = colorAndDescOfEyes;
        this.faceFrontPhoto = faceFrontPhoto;
        this.faceRightPhoto = faceRightPhoto;
        this.faceLeftPhoto = faceLeftPhoto;
        this.rightThumbFingerPrint = rightThumbFingerPrint;
        this.rightIndexFingerPrint = rightIndexFingerPrint;
        this.rightMiddleFingerPrint = rightMiddleFingerPrint;
        this.rightRingFingerPrint = rightRingFingerPrint;
        this.rightPinkyFingerPrint = rightPinkyFingerPrint;
        this.leftThumbFingerPrint = leftThumbFingerPrint;
        this.leftIndexFingerPrint = leftIndexFingerPrint;
        this.leftMiddleFingerPrint = leftMiddleFinderPrint;
        this.leftRingFingerPrint = leftRingFingerPrint;
        this.leftPinkyFingerPrint = leftPinkyFingerPrint;
    }

    public AddressDto getBirthPlace() {
        return birthPlace;
    }

    public CivilStatus getCivilStatus() {
        return civilStatus;
    }

    public AddressDto getCurrentAddress() {
        return currentAddress;
    }

    public AddressDto getCityAddress() {
        return cityAddress;
    }

    public AddressDto getProvincialAddress() {
        return provincialAddress;
    }

    public Name getSpouse() {
        return spouse;
    }

    public String getSpouseOccupation() {
        return spouseOccupation;
    }

    public AddressDto getSpouseAddress() {
        return spouseAddress;
    }

    public Name getFather() {
        return father;
    }

    public String getFathersOccupation() {
        return fathersOccupation;
    }

    public Name getMother() {
        return mother;
    }

    public String getMothersOccupation() {
        return mothersOccupation;
    }

    public AddressDto getParentsAddress() {
        return parentsAddress;
    }

    public Name getRelative1() {
        return relative1;
    }

    public Name getRelative2() {
        return relative2;
    }

    public String getDialects() {
        return dialects;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public String getHeight() {
        return height;
    }

    public String getWeight() {
        return weight;
    }

    public String getBuild() {
        return build;
    }

    public String getReligion() {
        return religion;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmergencyContactPerson() {
        return emergencyContactPerson;
    }

    public String getEmergencyContactPersonAddressAndNumber() {
        return emergencyContactPersonAddressAndNumber;
    }

    public String getHighestEducationalAttainment() {
        return highestEducationalAttainment;
    }

    public String getColorAndDescOfHair() {
        return colorAndDescOfHair;
    }

    public String getColorAndDescOfEyes() {
        return colorAndDescOfEyes;
    }
    
    

    public String getRightThumbFingerPrint() {
		return rightThumbFingerPrint;
	}

	public void setRightThumbFingerPrint(String rightThumbFingerPrint) {
		this.rightThumbFingerPrint = rightThumbFingerPrint;
	}

	public String getRightIndexFingerPrint() {
		return rightIndexFingerPrint;
	}

	public void setRightIndexFingerPrint(String rightIndexFingerPrint) {
		this.rightIndexFingerPrint = rightIndexFingerPrint;
	}

	public String getRightMiddleFingerPrint() {
		return rightMiddleFingerPrint;
	}

	public void setRightMiddleFingerPrint(String rightMiddleFingerPrint) {
		this.rightMiddleFingerPrint = rightMiddleFingerPrint;
	}

	public String getRightRingFingerPrint() {
		return rightRingFingerPrint;
	}

	public void setRightRingFingerPrint(String rightRingFingerPrint) {
		this.rightRingFingerPrint = rightRingFingerPrint;
	}

	public String getRightPinkyFingerPrint() {
		return rightPinkyFingerPrint;
	}

	public void setRightPinkyFingerPrint(String rightPinkyFingerPrint) {
		this.rightPinkyFingerPrint = rightPinkyFingerPrint;
	}

	public String getLeftThumbFingerPrint() {
		return leftThumbFingerPrint;
	}

	public void setLeftThumbFingerPrint(String leftThumbFingerPrint) {
		this.leftThumbFingerPrint = leftThumbFingerPrint;
	}

	public String getLeftIndexFingerPrint() {
		return leftIndexFingerPrint;
	}

	public void setLeftIndexFingerPrint(String leftIndexFingerPrint) {
		this.leftIndexFingerPrint = leftIndexFingerPrint;
	}

	public String getLeftMiddleFingerPrint() {
		return leftMiddleFingerPrint;
	}

	public void setLeftMiddleFingerPrint(String leftMiddleFingerPrint) {
		this.leftMiddleFingerPrint = leftMiddleFingerPrint;
	}

	public String getLeftRingFingerPrint() {
		return leftRingFingerPrint;
	}

	public void setLeftRingFingerPrint(String leftRingFingPrint) {
		this.leftRingFingerPrint = leftRingFingPrint;
	}

	public String getLeftPinkyFingerPrint() {
		return leftPinkyFingerPrint;
	}

	public void setLeftPinkyFingerPrint(String leftPinkyFingerPrint) {
		this.leftPinkyFingerPrint = leftPinkyFingerPrint;
	}
	
	public String getFaceFrontPhoto() {
		return faceFrontPhoto;
	}

	public void setFaceFrontPhoto(String faceFrontPhoto) {
		this.faceFrontPhoto = faceFrontPhoto;
	}

	public String getFaceRightPhoto() {
		return faceRightPhoto;
	}

	public void setFaceRightPhoto(String faceRightPhoto) {
		this.faceRightPhoto = faceRightPhoto;
	}

	public String getFaceLeftPhoto() {
		return faceLeftPhoto;
	}

	public void setFaceLeftPhoto(String faceLeftPhoto) {
		this.faceLeftPhoto = faceLeftPhoto;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonOfInterestDetailDto that = (PersonOfInterestDetailDto) o;

        if (birthPlace != null ? !birthPlace.equals(that.birthPlace) : that.birthPlace != null) return false;
        if (civilStatus != that.civilStatus) return false;
        if (currentAddress != null ? !currentAddress.equals(that.currentAddress) : that.currentAddress != null) return false;
        if (cityAddress != null ? !cityAddress.equals(that.cityAddress) : that.cityAddress != null) return false;
        if (provincialAddress != null ? !provincialAddress.equals(that.provincialAddress) : that.provincialAddress != null)
            return false;
        if (spouse != null ? !spouse.equals(that.spouse) : that.spouse != null) return false;
        if (spouseOccupation != null ? !spouseOccupation.equals(that.spouseOccupation) : that.spouseOccupation != null) return false;
        if (spouseAddress != null ? !spouseAddress.equals(that.spouseAddress) : that.spouseAddress != null) return false;
        if (father != null ? !father.equals(that.father) : that.father != null) return false;
        if (fathersOccupation != null ? !fathersOccupation.equals(that.fathersOccupation) : that.fathersOccupation != null)
            return false;
        if (mother != null ? !mother.equals(that.mother) : that.mother != null) return false;
        if (mothersOccupation != null ? !mothersOccupation.equals(that.mothersOccupation) : that.mothersOccupation != null)
            return false;
        if (parentsAddress != null ? !parentsAddress.equals(that.parentsAddress) : that.parentsAddress != null) return false;
        if (relative1 != null ? !relative1.equals(that.relative1) : that.relative1 != null) return false;
        if (relative2 != null ? !relative2.equals(that.relative2) : that.relative2 != null) return false;
        if (dialects != null ? !dialects.equals(that.dialects) : that.dialects != null) return false;
        if (citizenship != null ? !citizenship.equals(that.citizenship) : that.citizenship != null) return false;
        if (height != null ? !height.equals(that.height) : that.height != null) return false;
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) return false;
        if (build != null ? !build.equals(that.build) : that.build != null) return false;
        if (religion != null ? !religion.equals(that.religion) : that.religion != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (emergencyContactPerson != null ? !emergencyContactPerson.equals(that.emergencyContactPerson)
            : that.emergencyContactPerson != null) return false;
        if (emergencyContactPersonAddressAndNumber != null ? !emergencyContactPersonAddressAndNumber
            .equals(that.emergencyContactPersonAddressAndNumber) : that.emergencyContactPersonAddressAndNumber != null) return false;
        if (highestEducationalAttainment != null ? !highestEducationalAttainment.equals(that.highestEducationalAttainment)
            : that.highestEducationalAttainment != null) return false;
        if (colorAndDescOfHair != null ? !colorAndDescOfHair.equals(that.colorAndDescOfHair) : that.colorAndDescOfHair != null)
            return false;
        if (colorAndDescOfEyes != null ? !colorAndDescOfEyes.equals(that.colorAndDescOfEyes) : that.colorAndDescOfEyes != null)
            return false;
        if (faceFrontPhoto != null ? !faceFrontPhoto.equals(that.faceFrontPhoto) : that.faceFrontPhoto != null) return false;
        if (faceRightPhoto != null ? !faceRightPhoto.equals(that.faceRightPhoto) : that.faceRightPhoto != null) return false;
        if (faceLeftPhoto != null ? !faceLeftPhoto.equals(that.faceLeftPhoto) : that.faceLeftPhoto != null) return false;
        if (rightThumbFingerPrint != null ? !rightThumbFingerPrint.equals(that.rightThumbFingerPrint) : that.rightThumbFingerPrint != null) return false;
        if (rightIndexFingerPrint != null ? !rightIndexFingerPrint.equals(that.rightIndexFingerPrint) : that.rightIndexFingerPrint != null) return false;
        if (rightMiddleFingerPrint != null ? !rightMiddleFingerPrint.equals(that.rightMiddleFingerPrint) : that.rightMiddleFingerPrint != null) return false;
        if (rightRingFingerPrint != null ? !rightRingFingerPrint.equals(that.rightRingFingerPrint) : that.rightRingFingerPrint != null) return false;
        if (rightPinkyFingerPrint != null ? !rightPinkyFingerPrint.equals(that.rightPinkyFingerPrint) : that.rightPinkyFingerPrint != null) return false;
        if (leftThumbFingerPrint != null ? !leftThumbFingerPrint.equals(that.leftThumbFingerPrint) : that.leftThumbFingerPrint != null) return false;
        if (leftIndexFingerPrint != null ? !leftIndexFingerPrint.equals(that.leftIndexFingerPrint) : that.leftIndexFingerPrint != null) return false;
        if (leftMiddleFingerPrint != null ? !leftMiddleFingerPrint.equals(that.leftMiddleFingerPrint) : that.leftMiddleFingerPrint != null) return false;
        if (leftRingFingerPrint != null ? !leftRingFingerPrint.equals(that.leftRingFingerPrint) : that.leftRingFingerPrint != null) return false;
        return leftPinkyFingerPrint != null ? leftPinkyFingerPrint.equals(that.leftPinkyFingerPrint) : that.leftPinkyFingerPrint == null;

    }

    @Override
    public int hashCode() {
        int result = birthPlace != null ? birthPlace.hashCode() : 0;
        result = 31 * result + (civilStatus != null ? civilStatus.hashCode() : 0);
        result = 31 * result + (currentAddress != null ? currentAddress.hashCode() : 0);
        result = 31 * result + (cityAddress != null ? cityAddress.hashCode() : 0);
        result = 31 * result + (provincialAddress != null ? provincialAddress.hashCode() : 0);
        result = 31 * result + (spouse != null ? spouse.hashCode() : 0);
        result = 31 * result + (spouseOccupation != null ? spouseOccupation.hashCode() : 0);
        result = 31 * result + (spouseAddress != null ? spouseAddress.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        result = 31 * result + (fathersOccupation != null ? fathersOccupation.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (mothersOccupation != null ? mothersOccupation.hashCode() : 0);
        result = 31 * result + (parentsAddress != null ? parentsAddress.hashCode() : 0);
        result = 31 * result + (relative1 != null ? relative1.hashCode() : 0);
        result = 31 * result + (relative2 != null ? relative2.hashCode() : 0);
        result = 31 * result + (dialects != null ? dialects.hashCode() : 0);
        result = 31 * result + (citizenship != null ? citizenship.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (build != null ? build.hashCode() : 0);
        result = 31 * result + (religion != null ? religion.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (emergencyContactPerson != null ? emergencyContactPerson.hashCode() : 0);
        result =
            31 * result + (emergencyContactPersonAddressAndNumber != null ? emergencyContactPersonAddressAndNumber.hashCode() : 0);
        result = 31 * result + (highestEducationalAttainment != null ? highestEducationalAttainment.hashCode() : 0);
        result = 31 * result + (colorAndDescOfHair != null ? colorAndDescOfHair.hashCode() : 0);
        result = 31 * result + (colorAndDescOfEyes != null ? colorAndDescOfEyes.hashCode() : 0);
        result = 31 * result + (faceFrontPhoto != null ? faceFrontPhoto.hashCode() : 0);
        result = 31 * result + (faceRightPhoto != null ? faceRightPhoto.hashCode() : 0);
        result = 31 * result + (faceLeftPhoto != null ? faceLeftPhoto.hashCode() : 0);
        result = 31 * result + (rightThumbFingerPrint != null ? rightThumbFingerPrint.hashCode() : 0);
        result = 31 * result + (rightIndexFingerPrint != null ? rightIndexFingerPrint.hashCode() : 0);
        result = 31 * result + (rightMiddleFingerPrint != null ? rightMiddleFingerPrint.hashCode() : 0);
        result = 31 * result + (rightRingFingerPrint != null ? rightRingFingerPrint.hashCode() : 0);
        result = 31 * result + (rightPinkyFingerPrint != null ? rightPinkyFingerPrint.hashCode() : 0);
        result = 31 * result + (leftThumbFingerPrint != null ? leftThumbFingerPrint.hashCode() : 0);
        result = 31 * result + (leftIndexFingerPrint != null ? leftIndexFingerPrint.hashCode() : 0);
        result = 31 * result + (leftMiddleFingerPrint != null ? leftMiddleFingerPrint.hashCode() : 0);
        result = 31 * result + (leftRingFingerPrint != null ? leftRingFingerPrint.hashCode() : 0);
        result = 31 * result + (leftPinkyFingerPrint != null ? leftPinkyFingerPrint.hashCode() : 0);
        return result;
    }
}
