package org.r2.iris.clearance.service.poi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.r2.iris.clearance.service.common.AbstractAuditableEntity;
import org.r2.iris.clearance.service.poi.model.support.Gender;


/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@Entity
@Audited
public class PersonOfInterest extends AbstractAuditableEntity implements Serializable {
	private static final long serialVersionUID = 606834157710205375L;
	
    /**
	 * 
	 */
	@Id
    @GenericGenerator(name = "PersonIdentifierGenerator", strategy = "org.r2.iris.clearance.service.poi.model.PersonOfInterestIdGenerator")
    @GeneratedValue(generator = "PersonIdentifierGenerator")
    private String id;
    private String firstName;
    private String middleName;
    private String lastName;
    /**
     * Gender: M or F
     */
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private Date birthDate;
    private String alias;

    public PersonOfInterest() {
        // Default Constructor
    }

    public PersonOfInterest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonOfInterest that = (PersonOfInterest) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (gender != that.gender) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        return alias != null ? alias.equals(that.alias) : that.alias == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        return result;
    }
}