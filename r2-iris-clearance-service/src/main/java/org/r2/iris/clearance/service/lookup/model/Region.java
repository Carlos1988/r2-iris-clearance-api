package org.r2.iris.clearance.service.lookup.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
@Entity
public class Region extends AbstractAddress {
    @OneToMany(mappedBy = "region")
    private List<Province> provinces;

    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }
}
