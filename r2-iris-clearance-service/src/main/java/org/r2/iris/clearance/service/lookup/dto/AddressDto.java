package org.r2.iris.clearance.service.lookup.dto;

/**
 * @author Allan G. Ramirez (ramirezag@gmail.com)
 */
public class AddressDto {
    private String regionCode;
    private String regionName;
    private String provinceCode;
    private String provinceName;
    private String municipalityCode;
    private String municipalityName;
    private String barangayCode;
    private String barangayName;
    private String otherDetail;

    public AddressDto(String regionCode, String regionName, String provinceCode, String provinceName, String municipalityCode,
        String municipalityName, String barangayCode, String barangayName, String otherDetail) {
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
        this.municipalityCode = municipalityCode;
        this.municipalityName = municipalityName;
        this.barangayCode = barangayCode;
        this.barangayName = barangayName;
        this.otherDetail = otherDetail;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public String getMunicipalityCode() {
        return municipalityCode;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public String getBarangayCode() {
        return barangayCode;
    }

    public String getBarangayName() {
        return barangayName;
    }

    public String getOtherDetail() {
        return otherDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AddressDto that = (AddressDto) o;

        if (regionCode != null ? !regionCode.equals(that.regionCode) : that.regionCode != null) return false;
        if (provinceCode != null ? !provinceCode.equals(that.provinceCode) : that.provinceCode != null) return false;
        if (municipalityCode != null ? !municipalityCode.equals(that.municipalityCode) : that.municipalityCode != null) return false;
        if (barangayCode != null ? !barangayCode.equals(that.barangayCode) : that.barangayCode != null) return false;
        return otherDetail != null ? otherDetail.equals(that.otherDetail) : that.otherDetail == null;

    }

    @Override
    public int hashCode() {
        int result = regionCode != null ? regionCode.hashCode() : 0;
        result = 31 * result + (provinceCode != null ? provinceCode.hashCode() : 0);
        result = 31 * result + (municipalityCode != null ? municipalityCode.hashCode() : 0);
        result = 31 * result + (barangayCode != null ? barangayCode.hashCode() : 0);
        result = 31 * result + (otherDetail != null ? otherDetail.hashCode() : 0);
        return result;
    }
}
